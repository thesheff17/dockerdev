FROM ubuntu:20.04

# time docker build . -t thesheff17/dev

RUN echo 'Acquire::http { Proxy "http://172.17.0.2:3142"; };' >> /etc/apt/apt.conf.d/01proxy

LABEL status="keep"
RUN \
    export DEBIAN_FRONTEND="noninteractive" && \
    apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
    apt-utils \
    build-essential \
    curl \
    gcc \
    git \
    gnupg2 \
    htop \
    inetutils-ping \
    libbz2-dev \
    libedit-dev \
    libexpat1-dev \
    libffi-dev \
    libgc-dev \
    libgdbm-dev \
    libldap2-dev \
    liblzma-dev \
    libncurses5-dev \
    python3-openssl \
    libncursesw5-dev \
    libreadline-dev \
    libreadline8 \
    libsasl2-dev \
    libsqlite3-dev \
    libssl-dev \
    libxml2-dev \
    libxmlsec1-dev \
    llvm \
    make \
    net-tools \
    pkg-config \
    procps \
    python-cffi \
    python3-dev \
    software-properties-common \
    ssh \
    sshpass \
    supervisor \
    tk-dev \
    tmux \
    vim \
    wget \
    xz-utils \
    zlib1g-dev && \
    apt-get autoremove -y && \
    apt-get clean -y && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    yes | unminimize

RUN rm /etc/apt/apt.conf.d/01proxy

# vscode install
RUN wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg && \
    install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/ && \
    /bin/sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list' && \
    rm -f packages.microsoft.gpg && \
    apt-get update && \
    apt-get install -y code && \
    apt-get autoremove -y && \
    apt-get clean -y && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# configure some sshd settings
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN mkdir -p /run/sshd
RUN echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
RUN echo "AllowTcpForwarding yes" >> /etc/ssh/sshd_config

# node/yarn/npm
RUN \
    curl -o node.sh https://deb.nodesource.com/setup_16.x && \
    chmod +x node.sh && \
    ./node.sh && \
    apt-get install -y nodejs && \
    npm install --global yarn \
    npm install -g npm

# python3
RUN /bin/bash -c "curl https://pyenv.run | bash"
# RUN /bin/bash -c 'export PYTHON_CONFIGURE_OPTS="--enable-shared" && /root/.pyenv/bin/pyenv install pypy3.9-7.3.8'
RUN /bin/bash -c 'export PYTHON_CONFIGURE_OPTS="--enable-shared" && /root/.pyenv/bin/pyenv install 3.10.4'
RUN /bin/bash -c 'export PYTHON_CONFIGURE_OPTS="--enable-shared" && /root/.pyenv/bin/pyenv install 3.11-dev'
RUN ln -s /root/.pyenv/versions/3.10.4/bin/python3.10 /usr/bin/python3.10
RUN ln -s /root/.pyenv/versions/3.10.4/bin/python3.10 /usr/bin/python
RUN /bin/bash -c "python3.10 <(curl -s https://bootstrap.pypa.io/get-pip.py)"
RUN ln -s /root/.pyenv/versions/3.10.4/bin/pip /usr/bin/pip
RUN ln -s /root/.pyenv/bin/pyenv /usr/bin/pyenv
RUN /bin/bash -c "pyenv global 3.10.4"

# go
RUN wget --quiet https://dl.google.com/go/go1.8.linux-amd64.tar.gz
RUN tar -C /usr/local -xzf go1.8.linux-amd64.tar.gz
RUN echo 'export PATH=$PATH:/usr/local/go/bin' >> /root/.bashrc
RUN echo 'export GOBIN=/root/go/bin' >> /root/.bashrc
RUN echo 'export GOPATH=/root/go/bin' >> /root/.bashrc
RUN rm go1.8.linux-amd64.tar.gz

# ruby install
RUN mkdir ~/.gnupg
RUN echo "disable-ipv6" >> ~/.gnupg/dirmngr.conf
RUN gpg2 --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
RUN curl -sSL https://get.rvm.io | bash -s stable --ruby
RUN curl -sSL https://get.rvm.io | bash -s stable --rails

# rust
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y

CMD ["/bin/bash"]
